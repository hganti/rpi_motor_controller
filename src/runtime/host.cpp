/**
 * Server program for utilizing the RPi motor controller. It makes use of the DACs, ENCs, and TCP
 * servers available to it. It is designed for completely remote operation, so it doesn't output
 * to the console.
 * (c) Hari Ganti, 2013 <hariganti@gmail.com>
 *
 * As is, the program leaves the heavy calculations to the client. As a host device, it only
 * reports information back to the client upon request.
 *
 * An alternate design is to use the RPi (upon which this is based) as the workhorse, and collect,
 * process, and buffer data accordingly. This may be a better solution if the datasets are small
 * and the network experiences significant lag. Since this is designed for a direct Ethernet
 * connection, it assumes the network is sufficiently fast to allow for fast communication, so less
 * overhead is spent on calculations that the client can perform more quickly. Of course, if the
 * client is running a particularly heavy/intensive application, the reverse setup may be
 * preferable. Testing is required to determine the optimal flow.
 */

//Temporary Includes
#include <stdio.h>
#include <sys/time.h>
#include <unistd.h>

//Includes
#include <sched.h>
#include "../include/enc.h"
#include "../include/dac.h"
#include "../include/tcp.h"

//Constants
//Pins In Use
static int DAC_PINS[3] = {5, 6, 4}; //Pins for motor CS lines, should be 6, 5, 4
static int ENC_PINS[4] = {4, 0, 2, 3}; //Pins for encoder CS lines, should be 7, 0, 2, 3
//Codes
static unsigned char CODE_WAIT = 0x11; //Prefix for wait status
static unsigned char CODE_ERR = 0xFF; //Prefix for error status
//Math
static double COEFF_SHEAR = 1e5; //Torsion (nNm) vs angular displacement (rad)
static double RAD_PER_PLS = 2.5e-3; //Radians per encoder pulse

void resetDAC(DAC *dac, int numDacs) {
	for(int i = 0; i < numDacs; i++) {
		dac[i].reset();
//		printf("Resetting DACs\n");
	}

	return;
}

void resetENC(ENC *enc, int numEncs) {
	for(int i = 0; i < numEncs; i++) {
		enc[i].reset();
//		printf("Resetting ENCs\n");
	}

	return;
}

bool waitForSignal(TCP *tcp) {
	unsigned char ch[2];

	tcp->sendData(&CODE_WAIT, 1);
//	printf("Sent %.2X\n", ch[0]);
	tcp->recvData(ch, 2);
//	printf("Received %.2X\n", ch[0]);
	if(ch[0] == 0xAA)
		return true;

	//tcp->sendData(&CODE_ERR, 1);
	return false;
}

void collectData(TCP *tcp, int outlen, DAC *dac, int numDacs, ENC *enc, int numEncs) {
	unsigned char *cmdBuf; //Command buffer from the client computer
	unsigned char rawBuf[7 * numEncs]; //Input buffer from encoders
	int outBuf[outlen]; //Position and torque buffer
	int cmdlen;
	bool calculateTorques = false;
	timeval start, stop;

	if(numDacs == 2)
		cmdlen = 7;
	else if(numDacs == 3)
		cmdlen = 13;
	else
		return;

	cmdBuf = new unsigned char[cmdlen];

	if(numEncs >= numDacs)
		calculateTorques = true;

//	if(calculateTorques)
//		printf("Calculating torques...\n");

	memset(cmdBuf, 0, sizeof(cmdBuf));
	memset(rawBuf, 0, sizeof(rawBuf));
	memset(outBuf, 0, sizeof(outBuf));

	//Wait for start signal
	if(!waitForSignal(tcp))
		return;

	//Reset DACs and ENCs
	for(int i = 0; i < numEncs; i++) {
		enc[i].reset();
	}
	for(int i = 0; i < numDacs; i++) {
		dac[i].reset();
	}

	while(true) {
		//Wait for data request
		gettimeofday(&start, NULL);
		tcp->recvData(cmdBuf, cmdlen);
		gettimeofday(&stop, NULL);
//		printf("Recv: %ld\n", stop.tv_usec - start.tv_usec);
//		printf("%.2X\n", cmdBuf[0]);

/*		for(int i = 0; i < cmdlen; i++) {
			printf("%.2X ", cmdBuf[i]);
		} printf("\n");
*/
		gettimeofday(&start, NULL);

		switch(cmdBuf[0]) {
		  case 0x55:
		  	dac[0].setDAC(0, (uint16_t)((cmdBuf[1] << 8) | cmdBuf[2]));
		  	dac[0].setDAC(1, (uint16_t)((cmdBuf[3] << 8) | cmdBuf[4]));
		  	dac[1].setDAC(0, (uint16_t)((cmdBuf[5] << 8) | cmdBuf[6]));

		  	if(cmdlen < 12)
		  		break;

		  	dac[1].setDAC(1, (uint16_t)((cmdBuf[7] << 8) | cmdBuf[8]));
		  	dac[2].setDAC(0, (uint16_t)((cmdBuf[9] << 8) | cmdBuf[10]));
		  	dac[2].setDAC(1, (uint16_t)((cmdBuf[11] << 8) | cmdBuf[12]));
		  	break;

		  case 0xCC:
		  	resetENC(enc, numEncs);
			continue;
		  	break;

		  case 0xDD:
		  	resetDAC(dac, numDacs);
			continue;
		  	break;

		  case 0xFF:
//			printf("I should be exiting now\n");
		  	return;
			continue;
		  	break;

		  default:
		  	break;
		}

		//Read encoders
		for(int i = 0; i < numEncs; i++) {
			enc[i].readENC(&rawBuf[7 * i]);
//			printf("Reading encoders\n");
		}

		//Integrate positions
		for(int i = 0; i < numEncs; i++) {
			for(int j = 0; j < 3; j++) {
				outBuf[3 * i + j] = (int16_t)((rawBuf[7 * i + (2 * j + 1)] << 8) | rawBuf[7 * i + (2 * j + 2)]);
//				printf("Getting positions\n");
			}
		}

		//Calculate torques
		if(calculateTorques) {
			for(int i = numEncs / 2; i < numEncs; i++) {
				for(int j = 0; j < 3; j++) {
					outBuf[3 * i + j] = COEFF_SHEAR * RAD_PER_PLS * (outBuf[i] - outBuf[i - numEncs / 2]);
//					printf("Getting torques\n");
				}
			}
		}

		//Send data
		gettimeofday(&stop, NULL);
//		printf("Run: %ld\n", stop.tv_usec - start.tv_usec);

		gettimeofday(&start, NULL);
		tcp->sendData(outBuf, outlen);
		gettimeofday(&stop, NULL);
//		printf("Send: %ld\n", stop.tv_usec - start.tv_usec);
/*		printf("Sent! ");
		for(int i = 0; i < outlen; i++) {
			printf("%d ", outBuf[i]);
		}
		printf("\n");
*/
	}
}

int getOpMode(TCP *tcp) {
	unsigned char ch[2];

	tcp->sendData(&CODE_WAIT, 1);
	tcp->recvData(ch, 2);
//	printf("Received %.2X\n", ch[0]);

	switch(ch[0]) {
	  case 0x10:
	  	return 0;
	  	break;

	  case 0x11:
	  	return 1;
	  	break;

	  case 0x12:
		return 2;
		break;

	  case 0x13:
	  	return 3;
	  	break;

	  default:
	  	return -1;
	  	break;
	}

	return -1;
}

int main(int argc, char *argv[]) {
	//Declarations
	int outlen, numDacs, numEncs = 0;

	TCP *tcp = new TCP();
	DAC *dac;
	ENC *enc;

	//Get initial information
//	printf("Waiting for client\n");
	tcp->waitForClient();

	switch(getOpMode(tcp)) {
	  default:
	  case -1:
	  	tcp->sendData(&CODE_ERR, 1);
	  	return 1;
	  	break;

	  case 0:
	  	outlen = 3;
	  	numDacs = 2;
	  	numEncs = 1;
	  	break;

	  case 1:
	  	outlen = 6;
	  	numDacs = 2;
	  	numEncs = 2;
	  	break;

	  case 2:
	  	outlen = 6;
	  	numDacs = 3;
	  	numEncs = 2;
	  	break;

	  case 3:
	  	outlen = 12;
	  	numDacs = 3;
	  	numEncs = 4;
	  	break;
	}


	//Configure with parameters
	dac = new DAC[numDacs];
	enc = new ENC[numEncs];
//	printf("Set up SPI devices\n");

	for(int i = 0; i < numDacs; i++) {
		dac[i].setPin(DAC_PINS[i]);
	}
	for(int i = 0; i < numEncs; i++) {
		enc[i].setPin((ENC_PINS[i]));
	}

	//Set the runtime to high priority
	struct sched_param p;
	p.sched_priority = 99;
	sched_setscheduler(0, SCHED_FIFO, &p);

	//If all is good, begin the runtime loop
	collectData(tcp, outlen, dac, numDacs, enc, numEncs);

	//Cleanup
	for(int i = 0; i < numEncs; i++) {
		enc[i].reset();
	}
	for(int i = 0; i < numDacs; i++) {
		dac[i].reset();
	}

	delete tcp;
	delete[] dac;
	delete[] enc;
	return 0;
}
