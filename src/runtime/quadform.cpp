#include "../include/quadform.h"

QuadForm::QuadForm() {
	//Set up wiringPi Libraries
	wiringPiSetup();

	//Configure signal pins
	pinMode(A, OUTPUT);
	pinMode(B, OUTPUT);

	uSecs = 1000;
}

QuadForm::QuadForm(unsigned int uSecs_in) {
	uSecs = 4 * (unsigned int)(uSecs_in / 4);
}

void QuadForm::generate() {
	digitalWrite(A, HIGH);
	delayMicroseconds(uSecs / 4);
	digitalWrite(B, HIGH);
	delayMicroseconds(uSecs / 2);
	digitalWrite(A, LOW);
	delayMicroseconds(uSecs / 4);
	digitalWrite(B, LOW);

	return;
}
