#include "../include/remote.h"
#include <stdio.h>

Remote::Remote(const char *hostname, int mode) {
	//Initialize connection to BFRPi
	//Connect
	tcp.connectToServer(hostname);
	//Get ready signal
	tcp.recvData(cmdBuf, 1);
	//If the waiting signal isn't returned, this program should probably do something...
	//Set mode
	switch(mode) {
	  case 0:
	  	cmdlen = 3;
	  	datlen = 3;
	  	cmdBuf = new unsigned char[2 * cmdlen + 1];
	  	cmdBuf[0] = MODE_3M3E;
	  	break;

	  case 1:
	  	cmdlen = 3;
	  	datlen = 6;
	  	cmdBuf = new unsigned char[2 * cmdlen + 1];
	  	cmdBuf[0] = MODE_3M6E;
	  	break;

	  case 2:
	  	cmdlen = 6;
	  	datlen = 6;
	  	cmdBuf = new unsigned char[2 * cmdlen + 1];
	  	cmdBuf[0] = MODE_6M6E;
	  	break;

	  case 3:
	  	cmdlen = 6;
	  	datlen = 12;
	  	cmdBuf = new unsigned char[2 * cmdlen + 1];
	  	cmdBuf[0] = MODE_6M12E;
	  	break;

	  default:
	  	return;
	  	break;
	}

	cmdBuf[1] = NO_OP;
	tcp.sendData(cmdBuf, 2);

	memset(cmdBuf, 0, sizeof(cmdBuf));
}

Remote::~Remote() {
	delete cmdBuf;
}

void Remote::start() {
	//Get waiting signal
	tcp.recvData(cmdBuf, 1);
	//If the waiting signal isn't returned, this program should probably do something...

	cmdBuf[0] = CMD_START;
	cmdBuf[1] = NO_OP;
	tcp.sendData(cmdBuf, 2);

	return;
}

void Remote::shutdown() {
	cmdBuf[0] = CMD_SHDN;
	cmdBuf[1] = NO_OP;
	tcp.sendData(cmdBuf, 2);

	return;
}

void Remote::resetDAC() {
	cmdBuf[0] = CMD_RST_DAC;
	cmdBuf[1] = NO_OP;
	tcp.sendData(cmdBuf, 2);
}

void Remote::resetENC() {
	cmdBuf[0] = CMD_RST_ENC;
	cmdBuf[1] = NO_OP;
	tcp.sendData(cmdBuf, 2);
}

void Remote::readWrite(uint16_t *codes, int *data) {
	cmdBuf[0] = CMD_RW;
	for(int i = 0; i < cmdlen; i++) {
		cmdBuf[2 * i + 1] = (unsigned char)((codes[i] >> 8) & 0x0F);
		cmdBuf[2 * i + 2] = (unsigned char)(codes[i] & 0xFF);
//		printf("%.2X %.2X\n", cmdBuf[2 * i + 1], cmdBuf[2 * i + 2]);
	}

	tcp.sendData(cmdBuf, 2 * cmdlen + 1);
	tcp.recvData(data, datlen);

	return;
}
