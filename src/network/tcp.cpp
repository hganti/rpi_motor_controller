/**
 * TODO:
 * Reimplement the conversions from int * to unsigned char * in the calling function to avoid constructing new objects.
 */


#include "../include/tcp.h"
#include <stdio.h>

static void sigchld_handler(int s) {
    while(waitpid(-1, NULL, WNOHANG) > 0);
}

TCP::TCP() {
	;
}

TCP::~TCP() {
	;
}

void TCP::charToInt(unsigned char *buf, int len, int *out) {
	for(int i = 0; i < len / 4; i++) {
//		printf("Received %.2X %.2X %.2X %.2X\n", buf[4 * i], buf[4 * i + 1], buf[4 * 1 + 2], buf[4 * i + 3]);
		out[i] = (int)((buf[4 * i] << 24) | ((buf[4 * i + 1] << 16) | ((buf[4 * i + 2] << 8) | (buf[4 * i + 3] & 0xFF))));
	}

	return;
}

void TCP::intToChar(int *buf, int len, unsigned char *out) {
	for(int i = 0; i < len; i++) {
		out[4 * i] = (unsigned char)(buf[i] >> 24);
		out[4 * i + 1] = (unsigned char)(buf[i] >> 16);
		out[4 * i + 2] = (unsigned char)(buf[i] >> 8);
		out[4 * i + 3] = (unsigned char)(buf[i] & 0xFF);
	}

	return;
}

bool TCP::waitForClient() {
	int tempfd;
	struct addrinfo hints, *servinfo, *p;
	struct sockaddr_storage their_addr;
	struct sigaction sa;
	int yes = 1;
	socklen_t size = sizeof(their_addr);

	memset(&hints, 0, sizeof(hints));
	hints.ai_family = AF_UNSPEC;
	hints.ai_socktype = SOCK_STREAM;
	hints.ai_flags = AI_PASSIVE;

	if(getaddrinfo(NULL, PORT, &hints, &servinfo) != 0)
		return false;

	for(p = servinfo; p != NULL; p = p->ai_next) {
		if((tempfd = socket(p->ai_family, p->ai_socktype, p->ai_protocol)) == -1)
			continue;

		if(bind(tempfd, p->ai_addr, p->ai_addrlen) == -1)
			continue;

		break;
	}

	if(p == NULL)
		return false;

	if(setsockopt(tempfd, SOL_SOCKET, SO_REUSEADDR, &yes, sizeof(int)) == -1)
		return false;
	freeaddrinfo(servinfo);

	if(listen(tempfd, BACKLOG) == -1)
		return false;

	sa.sa_handler = sigchld_handler;
	sigemptyset(&sa.sa_mask);
    sa.sa_flags = SA_RESTART;
    if (sigaction(SIGCHLD, &sa, NULL) == -1)
    	return false;

    if((sockfd = accept(tempfd, (struct sockaddr *)&their_addr, &size)) == -1)
    	return false;

	close(tempfd);
    return true;
}

bool TCP::connectToServer(const char *hostname) {
	struct addrinfo hints, *servinfo, *p;

	memset(&hints, 0, sizeof(hints));
	hints.ai_family = AF_UNSPEC;
	hints.ai_socktype = SOCK_STREAM;

	if(getaddrinfo(hostname, PORT, &hints, &servinfo) != 0)
		return false;

	for(p = servinfo; p != NULL; p = p->ai_next) {
		if((sockfd = socket(p->ai_family, p->ai_socktype, p->ai_protocol)) == -1)
			return false;

		if(connect(sockfd, p->ai_addr, p->ai_addrlen) == -1) {
			close(sockfd);
			continue;
		}

		break;
	}

	if(p == NULL)
		return false;
	freeaddrinfo(servinfo);

	return true;
}

bool TCP::sendData(int *buf, int len) {
	unsigned char *newBuf = new unsigned char[4 * len];
//	memcpy(newBuf, &buf, sizeof(newBuf));
	intToChar(buf, len, &newBuf);

/*	for(int i = 0; i < 4 * len; i++) {
		printf("%.2X ", newBuf[i]);
	} printf("\n");
*/
	//if(!fork()) {
		if(send(sockfd, &newBuf, 4 * len, 0) == -1)
			return false;
		exit(0);
	//}
	
	return true;
}

bool TCP::sendData(unsigned char *buf, int len) {
	//if(!fork()) {
		if(send(sockfd, buf, len, 0) == -1)
			return false;
		exit(0);
	//}

/*	for(int i = 0; i < len; i++) {
		printf("%.2X ", buf[i]);
	} printf("\n");
*/
	return true;
}

bool TCP::recvData(int *buf, int len) {
	unsigned char *newBuf = new unsigned char[4 * len + 1];

	if(recv(sockfd, newBuf, 4 * len + 1, 0) == -1)
		return false;

/*	memcpy(&buf, &newBuf, sizeof(buf));

	printf("Received %d %d %d %d %d\n", newBuf[4], newBuf[3], newBuf[2], newBuf[1], newBuf[0]);
	printf("Which translates to %d\n", buf[0]);
*/
	charToInt(newBuf, 4 * len, buf);
/*	printf("Received ");
	for(int i = 0; i < len; i++) {
		printf("%d ", buf[i]);
	}
	printf("\n");
*/
	delete[] newBuf;
	return true;
}

bool TCP::recvData(unsigned char *buf, int len) {
	if(!recv(sockfd, buf, len, 0) == -1)
		return false;

/*	for(int i = 0; i < len; i++) {
		printf("%.2X ", buf[i]);
	} printf("\n");

*/	return true;
}

void TCP::closeSocket() {
	close(sockfd);
	return;
}
