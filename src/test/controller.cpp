/**
 * Control program to utilize the DACs and ENCs
 */

//Includes
#include <stdio.h>
#include <pthread.h>
#include <stdint.h>
#include <string.h>
#include <math.h>
#include <sys/time.h>

#include "../include/enc.h"
#include "../include/dac.h"
#include "../include/quadform.h"
#include "../include/tcp.h"

//Define constants
#define BUFLEN 3

static bool run;
static bool toggleSignal;
static bool toggleServer;
static char tcpBuf[2 * BUFLEN];
static int incBuf[BUFLEN];
static long cumBuf[BUFLEN];
static pthread_mutex_t lock;

void *quadSignal(void *arg) {
	QuadForm quadform;
	while(run) {
		if(!toggleSignal)
			continue;

		quadform.generate();
	}

	pthread_exit(NULL);
}
/*
void *tcp(void *arg) {
	TCPComm tcp;
	tcp.waitForClient();

	while(run) {
		if(!toggleServer)
			continue;

		tcp.sendReceive(tcpBuf, 2 * BUFLEN);
	}

	tcp.closeSocket();
	pthread_exit(NULL);
}
*/
void *readENC(void *arg) {
	ENC enc(4);
	unsigned char buf[7];

	enc.reset();
	while(run) {
		for(int i = 0; i < 1000; i++) {
			pthread_mutex_lock(&lock);
			enc.readENC(buf);
			for(int i = 0; i < BUFLEN; i++) {
				int16_t val = (int16_t)((buf[2 * i + 1] << 8) | buf[2 * i + 2]);
				tcpBuf[2 * i] = buf[2 * i + 1];
				tcpBuf[2 * i + 1] = buf[2 * i + 2];
				incBuf[i] += val;
				cumBuf[i] += val;
			}
			pthread_mutex_unlock(&lock);
		}
	}

	enc.reset();
	pthread_exit(NULL);
}

void *control(void *arg) {
	DAC dac(5);
	char str[2];
	int chan = -1;
	uint16_t code = -1;
	timeval start, stop;

	while(run) {
		printf("Enter a command: ");
		scanf("%s", str);

		switch(str[0]) {
		  case 'h':
			printf("The commands in use are:\n"
				"h - help\n"
				"e - run experiment\n"
				"s - set DACs\n"
				"r - read ENCs (incremental)\n"
				"t - read ENCs (cumulative)\n"
				"i - Toggle the data server\n"
				"q - Toggle the quadrature waveform\n"
				"x - exit\n");
		  	break;

		  case 'e':
		  	printf("Current experiments include:\n"
		  		"s - sine wave generator\n"
		  		"e - encoder read test\n");
		  	scanf("%s", str);

		  	switch(str[0]) {
		  	  case 's':
				dac.reset();
				printf("Generating 10 sine waves\n");
				for(int i = 0; i < 10; i++) {
					for(double d = 0; d < (2 * 3.1415926536); d += 0.001) {
						pthread_mutex_lock(&lock);
						dac.setDAC(0, (2048 * sin(d)) + 2048);
						pthread_mutex_unlock(&lock);
					}
				}
				break;

			  case 'e':
			  	printf("Displaying changes to encoder readings\n");
				gettimeofday(&start, NULL);
				gettimeofday(&stop, NULL);
				while(stop.tv_sec - start.tv_sec < 60) {
					pthread_mutex_lock(&lock);
			  		for(int i = 0; i < BUFLEN; i++) {
						printf("%s%d%s%-6d\t", "Encoder ", i, ": ", incBuf[i]);
						incBuf[i] = 0;
			  		}
					printf("\r"); fflush(NULL);
					pthread_mutex_unlock(&lock);
					gettimeofday(&stop, NULL);
					delay(100);
				}
				break;

			  default:
			  	printf("Unrecognized command\n");
			  	break;

			}
			break;

		  case 's':
			chan = code = -1;

			printf("Enter the DAC channel: ");
			scanf("%d", &chan);

			printf("Enter the value: ");
			scanf("%hu", &code);

			//Safety check, adjust accordingly
			if(code > 2559)
				code = 2559;
			if(code < 1535)
				code = 1535;

			pthread_mutex_lock(&lock);
			dac.setDAC(chan, code);
			pthread_mutex_unlock(&lock);
			break;

		  case 'r':
			pthread_mutex_lock(&lock);
		  	for(int i = 0; i < BUFLEN; i++) {
				if(i % 3 == 0)
					printf("\n");
		  		printf("Encoder %d: %d\t", i, incBuf[i]);
		  		incBuf[i] = 0;
		  	}
			printf("\n");

		  	pthread_mutex_unlock(&lock);
		  	break;

		  case 't':
		  	pthread_mutex_lock(&lock);
		  	for(int i = 0; i < BUFLEN; i++) {
				if(i % 3 == 0)
					printf("\n");
		  		printf("Encoder %d: %ld\t", i, cumBuf[i]);
		  	}
			printf("\n");

		  	pthread_mutex_unlock(&lock);
		  	break;

		  case 'q':
			toggleSignal = !toggleSignal;
			if(toggleSignal)
				printf("Quadrature waveform enabled\n");
			else
				printf("Quadrature waveform disabled\n");

			break;

		  case 'x':
			run = false;
			break;
		  default:
		  	printf("Unrecognized command\n");
		  	break;
		}
	}

	dac.reset();
	exit(0);
	pthread_exit(NULL);
}

int main(int argc, char *argv[]) {
	//Setup
	run = true;
	toggleSignal = false;
	toggleServer = false;

	memset(tcpBuf, 0, sizeof(tcpBuf));
	memset(incBuf, 0, sizeof(incBuf));
	memset(cumBuf, 0, sizeof(cumBuf));

	//Print instructions
	printf("The commands in use are:\n"
				"h - help\n"
				"e - run experiment\n"
				"s - set DACs\n"
				"r - read ENCs (incremental)\n"
				"t - read ENCs (cumulative)\n"
				"i - Toggle the data server\n"
				"q - Toggle the quadrature waveform\n"
				"p - Toggle the timing display\n"
				"x - exit\n");

	//Make threads and run
	pthread_t tcp_thr, sig_thr, enc_thr, con_thr;

/*	if(pthread_create(&tcp_thr, NULL, tcp, NULL) != 0) {
		printf("Failed to create tcp_thr\n");
		return 1;
	}
*/	if(pthread_create(&sig_thr, NULL, quadSignal, NULL) != 0) {
		printf("Failed to create sig_thr\n");
		return 1;
	}
	if(pthread_create(&enc_thr, NULL, readENC, NULL) != 0) {
		printf("Failed to create enc_thr\n");
		return 1;
	}
	if(pthread_create(&con_thr, NULL, control, NULL) != 0) {
		printf("Failed to create con_thr\n");
		return 1;
	}
	pthread_mutex_init(&lock, NULL);

//	pthread_join(tcp_thr, NULL);
	pthread_join(sig_thr, NULL);
	pthread_join(enc_thr, NULL);
	pthread_join(con_thr, NULL);


	pthread_mutex_destroy(&lock);
	return 0;
}
