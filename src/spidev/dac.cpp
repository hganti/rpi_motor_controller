#include "../include/dac.h"

DAC::DAC() {
	;
}

DAC::DAC(int pin_in) {
	pin = pin_in;

	//Set up wiringPi libraries
	if(wiringPiSetup() == -1)
		printf("Failed to initialize wiringPi (@dac.cpp)\n");
	if(wiringPiSPISetup(0, 10000000) == -1)
		printf("Failed to initialize wiringPiSPI (@dac.cpp)\n");

	//Configure the CS_PIN
	pinMode(pin, OUTPUT);
	digitalWrite(pin, 1);

	reset();
}

DAC::~DAC() {
	reset();
}

void DAC::setPin(int pin_in) {
	pin = pin_in;

	//Set up wiringPi libraries
	if(wiringPiSetup() == -1)
		printf("Failed to initialize wiringPi (@dac.cpp)\n");
	if(wiringPiSPISetup(0, 7500000) == -1)
		printf("Failed to initialize wiringPiSPI (@dac.cpp)\n");

	//Configure the CS_PIN
	pinMode(pin, OUTPUT);
	digitalWrite(pin, 1);

	reset();
	return;
}

void DAC::transmit(unsigned char *ch, int len) {
	//Send the data in ch
	digitalWrite(pin, 0);
	wiringPiSPIDataRW(0, ch, len);
	digitalWrite(pin, 1);

	return;
}

bool DAC::reset() {
	unsigned char ch[2] = {WRA_ADDR | ((uint16_t)2048 >> 8), (uint16_t)2048 & 0xFF };

	//Send the reset command
	transmit(ch, 2);

	ch[0] = WRB_ADDR | ((uint16_t)2048 >> 8);
	ch[1] = (uint16_t)2048 & 0xFF;

	//Send the reset command
	transmit(ch, 2);

	return true;
}

void DAC::setDAC(int chan, uint16_t code) {
	if((chan == -1) || (code == -1))
		return;
	if(code < 0)
		code = 0;
	if(code > 4095)
		code = 4095;

	unsigned char ch[2];

	//Send the write command to the appropriate channel
	if(chan == 0) {
		ch[0] = (WRA_ADDR | (code >> 8));
		ch[1] = (unsigned char)(code & 0xFF);

		transmit(ch, 2);
	} else if(chan == 1) {
		ch[0] = (WRB_ADDR | (code >> 8));
		ch[1] = (unsigned char)(code & 0xFF);

		transmit(ch, 2);
	}

//	printf("%.2X %.2X\n", ch[0], ch[1]);

	return;
}
