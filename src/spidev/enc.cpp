#include "../include/enc.h"

ENC::ENC() {
	;
}

ENC::ENC(int pin_in) {
	pin = pin_in;
	unsigned char ch[2];

	//Set up wiringPi libraries
	if(wiringPiSetup() == -1)
		printf("Failed to initialize wiringPi (@enc.cpp)\n");
	if(wiringPiSPISetup(0, 10000000) == -1)
		printf("Failed to initialize wiringPiSPI (@enc.cpp)\n");

	//Configure the CS_PIN
	pinMode(pin, OUTPUT);
	digitalWrite(pin, 1);

	//Configure the encoder
	ch[0] = (WR | CNT_ADDR);
	ch[1] =  CNT_CFG;
	transmit(ch, 2);

	ch[0] = (WR | TTL_ADDR);
	ch[1] =  TTL_CFG;
	transmit(ch, 2);

	ch[0] = (WR | RST_ADDR);
	ch[1] = RST_CMD;
	transmit(ch, 2);

	reset();
}

ENC::~ENC() {
	reset();
}

void ENC::setPin(int pin_in) {
    pin = pin_in;
	unsigned char ch[2];

	//Set up wiringPi libraries
	if(wiringPiSetup() == -1)
		printf("Failed to initialize wiringPi (@enc.cpp)\n");
	if(wiringPiSPISetup(0, 7500000) == -1)
		printf("Failed to initialize wiringPiSPI (@enc.cpp)\n");

	//Configure the CS_PIN
	pinMode(pin, OUTPUT);
	digitalWrite(pin, 1);

	//Configure the encoder
	ch[0] = (WR | CNT_ADDR);
	ch[1] =  CNT_CFG;
	transmit(ch, 2);

	ch[0] = (WR | TTL_ADDR);
	ch[1] =  TTL_CFG;
	transmit(ch, 2);

	ch[0] = (WR | RST_ADDR);
	ch[1] = RST_CMD;
	transmit(ch, 2);

	reset();

	return;
}

void ENC::transmit(unsigned char *ch, int len) {
	//Send the data in ch
	digitalWrite(pin, 0);
	wiringPiSPIDataRW(0, ch, len);
	digitalWrite(pin, 1);

	return;
}

bool ENC::reset() {
	unsigned char ch[2] = {WR | RST_ADDR, RST_CMD};

	//Send reset command
	transmit(ch, 2);

	return true;
}

void ENC::readENC(unsigned char *buf) {
	buf[0] = (RD | VAL_ADDR);
	buf[1] = buf[2] = buf[3] = buf[4] = buf[5] = buf[6] = 0x00;

	//Send read command
	transmit(buf, 7);

	//Reset the buffers
	reset();

	return;
}
