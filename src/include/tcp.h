/**
 * Class for simultaneous send/receive transactions using TCP sockets. More or less just Beej.
 * (c) Hari Ganti, 2013 <hariganti@gmail.com>
 */

#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <string.h>
#include <sys/types.h>
#include <sys/socket.h>
#include <netinet/in.h>
#include <netdb.h>
#include <arpa/inet.h>
#include <sys/wait.h>
#include <signal.h>

#define PORT "5000"

#define BACKLOG 10

class TCP {
	int sockfd;
	struct sockaddr_storage their_addr;
	void charToInt(unsigned char *buf, int len, int *out);
	void intToChar(int *buf, int len, unsigned char *out);
  public:
  	TCP();
  	~TCP();
  	bool waitForClient();
  	bool connectToServer(const char *hostname);
  	bool sendData(int *buf, int len);
  	bool sendData(unsigned char *buf, int len);
  	bool recvData(int *buf, int len);
  	bool recvData(unsigned char *buf, int len);
	void closeSocket();
};
