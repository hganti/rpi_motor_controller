/**
 * Class for writing analog voltages with the MAX5322 DAC (DAC)
 * (c) Hari Ganti, 2013 <hariganti@gmail.com>
 */

//Includes
#include <stdint.h>
#include <stdio.h>

//wiringPi Libraries
#include <wiringPi.h>
#include <wiringPiSPI.h>

//Addresses
#define WRA_ADDR 0b01000000
#define WRB_ADDR 0b01010000

class DAC {
	int pin;
	void transmit(unsigned char *ch, int len);
  public:
  	DAC();
  	DAC(int pin_in);
  	~DAC();
  	void setPin(int pin_in);
  	bool reset();
  	void setDAC(int chan, uint16_t code);
};
